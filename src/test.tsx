import * as React from 'react';
import * as ReactDOM from 'react-dom';


let greeting:string = "Hello World"
//console.log("greeting: " + greeting);

function add(a:number, b:number){
    return a + b;
}

//console.log("add: " + add(3,6));

class NumberBox {
    num:number;


    constructor(num:number) {
        this.num = num;
        console.log("numberBox created: " + num);
    }

    render() {
        return(
            <div>
                <p>{this.num}</p>
            </div>
        );
        
    }
        
    

}

//var n = new NumberBox(5);