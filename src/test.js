"use strict";
exports.__esModule = true;
var React = require("react");
var greeting = "Hello World";
//console.log("greeting: " + greeting);
function add(a, b) {
    return a + b;
}
//console.log("add: " + add(3,6));
var NumberBox = /** @class */ (function () {
    function NumberBox(num) {
        this.num = num;
        console.log("numberBox created: " + num);
    }
    NumberBox.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("p", null, this.num)));
    };
    return NumberBox;
}());
//var n = new NumberBox(5);
